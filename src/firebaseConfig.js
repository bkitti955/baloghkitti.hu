import firebase from 'firebase/app';

const config = {
  apiKey: "AIzaSyCjZ_LF-KxT2-KNwX-xwf9K5oo1I5tMEpU",
  authDomain: "personal-site-f9adc.firebaseapp.com",
  databaseURL: "https://personal-site-f9adc.firebaseio.com",
  projectId: "personal-site-f9adc",
  storageBucket: "personal-site-f9adc.appspot.com",
  messagingSenderId: "704024536640",
  appId: "1:704024536640:web:91022d6c1c6ea73a6a61a5",
  measurementId: "G-S1JRQZ9SGE"
}
firebase.initializeApp(config);

export default {};
