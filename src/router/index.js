import Vue from "vue";
import VueRouter from "vue-router";
import Main from "../views/Main.vue";
import Home from "../views/Home.vue";
import NotFound from "../components/NotFound.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Main",
    component: Main
  },
  {
    path: "/home",
    name: "Home",
    component: Home
  },
  { path: '*', component: NotFound }
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
